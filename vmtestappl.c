#include "vmtestappl.h"
#include "vmem.h"
#include "mmanage.h"

#define RUNS (3 * VMEM_PAGESIZE + 1)

void test1(void) {
    int i = 0;

    for (i = 0; i < RUNS; i++) {
        vmem_write(i, i);
    }
}

void test2(void) {
    int i = 0;

    for (i = 0; i < VMEM_NFRAMES + 1; i++) {
        vmem_write(VMEM_PAGESIZE * i, 0);

    }
}

int main(void) {

    test2();

    return 0;
}
