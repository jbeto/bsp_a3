# Makefile für BSP A3 - Virtuelle Speicherverwaltung

# Wenn nicht definiert soll gesetzt werden
PAGESIZE ?= 8
ALGO ?= 0
AGING ?= 20

CC      = gcc
CFLAGS  = -Wall -g3 -Wextra
CFLAGS += -D'VMEM_PAGESIZE=$(PAGESIZE)'
CFLAGS += -D'VMEM_ALGO=$(ALGO)'
CFLAGS += -D'AGING_AGE=$(AGING)'
#CFLAGS += -DDEBUG_MESSAGES
LDFLAGS = -pthread

# ALGO_FLAG_FIFO 0
# ALGO_FLAG_AGING 1
# ALGO_FLAG_CLOCK 2

VMAPPL = vmapp1
VMAPPLSRC = vmappl.c vmaccess.c
VMAPPLOBJ = $(VMAPPLSRC:%.c=%.o)
VMTESTAPPL = vmtestappl
VMTESTAPPLSRC = vmtestappl.c vmaccess.c
VMTESTAPPLOBJ = $(VMTESTAPPLSRC:%.c=%.o)
MMANAGE = mmanage
MMANAGESRC = mmanage.c
MMANAGEOBJ = $(MMANAGESRC:%.c=%.o)

OBJS    = $(SRC:%.c=%.o)
SRC	= $(VMAPPLSRC) $(VMTESTAPPLSRC) $(MMANAGESRC)
PROG	= $(VMAPPL) $(VMTESTAPPL) $(MMANAGE)
DEPENDFILE = .depend

all: $(PROG)

$(VMAPPL) : $(VMAPPLOBJ)
	$(CC) $(CFLAGS) -o $(VMAPPL) $(VMAPPLOBJ) $(LDFLAGS)

$(VMTESTAPPL) : $(VMTESTAPPLOBJ)
	$(CC) $(CFLAGS) -o $(VMTESTAPPL) $(VMTESTAPPLOBJ) $(LDFLAGS)

$(MMANAGE) : $(MMANAGEOBJ)
	$(CC) $(CFLAGS) -o $(MMANAGE) $(MMANAGEOBJ) $(LDFLAGS)


%.o: %.c
	$(CC) $(CFLAGS) -c $<

dep: $(SRC)
	$(CC) -MM $(SRC) > $(DEPENDFILE)

-include $(DEPENDFILE)

.PHONY: clean
clean:
	rm -rf $(OBJS)

.PHONY: fullclean
fullclean:
	rm -rf $(PROG) $(OBJS)

rebuild: fullclean all
