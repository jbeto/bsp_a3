#!/bin/bash
# Führt alle Algorithmen aus und speichert die erstellen
#
# Edgar Toll
# mit besten Dank an Daniel Sarnow für die Inspiration
# 25.11.14

pagesizes=( 8 16 32 64 )
algos=( 0 1 2 )
algoNames=( FIFO AGING CLOCK )
agingTimes=( 5 20 100 )

if [ ! -d "logs" ]
then
    mkdir logs
fi

for algo in "${algos[@]}"
do
    for pagesize in "${pagesizes[@]}"
    do
        for agingTime in "${agingTimes[@]}"
        do
            path="${algoNames[$algo]}_${pagesize}"
            makeparameter="ALGO=$algo "
            makeparameter+="PAGESIZE=$pagesize "

            if [ $algo -eq 1 ]
            then
                path+="_${agingTime}"
                makeparameter+="AGING=$agingTime "
            fi

            echo $path
            path="logs/"${path}

            if [ ! -d "${path}" ]
            then
                mkdir ${path}
            fi

            make rebuild $makeparameter > ${path}/make.log
            ./mmanage &> ${path}/mmanage.out &
            sleep 1 && ./vmapp1 &> ${path}/vmapp1.out
            mv -f logfile.txt ${path}/
            mv -f pagefile.bin ${path}/

            if [ ! $algo -eq 1 ]
            then
                diff -q godlogs/logfile_${algoNames[$algo]}_${pagesize}.txt ${path}/logfile.txt
                break
            fi
        done
    done
    echo
done

make fullclean

exit 0
