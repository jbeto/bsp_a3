/* Description: Memory Manager BSP3*/
/* Prof. Dr. Wolfgang Fohl, HAW Hamburg */
/* Sommer 2014
 *
 * This is the memory manager process that
 * works together with the vmaccess process to
 * mimic virtual memory management.
 *
 * The memory manager process will be invoked
 * via a SIGUSR1 signal. It maintains the page table
 * and provides the data pages in shared memory
 *
 * This process is initiating the shared memory, so
 * it has to be started prior to the vmaccess process
 * */

#include "mmanage.h"
#include <stdlib.h>
#include <limits.h>
#include <signal.h>

void logThisEvent(int replaced_page, int alloc_frame);

struct vmem_struct *vmem = NULL;
FILE *pagefile = NULL;
FILE *logfile = NULL;
int signal_number = 0; /* Received signal */
int vmem_algo = VMEM_ALGO_FIFO;

int
main(void) {
    struct sigaction sigact;

    /* Init pagefile */
    init_pagefile(MMANAGE_PFNAME);
    if (!pagefile) {
        perror("\e[0;31mError creating pagefile\e[m");
        exit(EXIT_FAILURE);
    }

    /* Delete old logfile */
    if (remove(MMANAGE_LOGFNAME) == 0) {
#ifdef DEBUG_MESSAGES
        fprintf(stderr, "\e[1;32mlogfile deleted\e[m\n");
    } else {
        fprintf(stderr, "\e[1;33mlogfile could not deleted - nothing wrong\e[m\n");
#endif /* DEBUG_MESSAGES */
    }

    /* Open logfile */
    logfile = fopen(MMANAGE_LOGFNAME, "w");
    if (!logfile) {
        perror("\e[0;31mError creating logfile\e[m");
        exit(EXIT_FAILURE);
    }

    /* Create shared memory and init vmem structure */
    vmem_init();
    if (!vmem) {
        perror("\e[0;31mError initialising vmem\e[m");
        exit(EXIT_FAILURE);
    }
#ifdef DEBUG_MESSAGES
    else {
        fprintf(stderr, "\e[1;32mvmem successfully created\e[m\n");
    }
#endif /* DEBUG_MESSAGES */

    /* Setup signal handler */
    /* Handler for USR1 */
    sigact.sa_handler = sighandler;
    sigemptyset(&sigact.sa_mask);
    sigact.sa_flags = 0;
    if (sigaction(SIGUSR1, &sigact, NULL) == -1) {
        perror("\e[0;31mError installing signal handler for USR1\e[m");
        exit(EXIT_FAILURE);
    }
#ifdef DEBUG_MESSAGES
    else {
        fprintf(stderr, "\e[1;32mUSR1 handler successfully installed\e[m\n");
    }
#endif /* DEBUG_MESSAGES */

    if (sigaction(SIGUSR2, &sigact, NULL) == -1) {
        perror("\e[0;31mError installing signal handler for USR2\e[m");
        exit(EXIT_FAILURE);
    }
#ifdef DEBUG_MESSAGES
    else {
        fprintf(stderr, "\e[1;32mUSR2 handler successfully installed\e[m\n");
    }
#endif /* DEBUG_MESSAGES */

    if (sigaction(SIGINT, &sigact, NULL) == -1) {
        perror("\e[0;31mError installing signal handler for INT\e[m");
        exit(EXIT_FAILURE);
    }
#ifdef DEBUG_MESSAGES
    else {
        fprintf(stderr, "\e[1;32mINT handler successfully installed\e[m\n");
    }
#endif /* DEBUG_MESSAGES */

    /* Signal processing loop */
    while (1) {
        signal_number = 0;
        pause();
        if (signal_number == SIGUSR1) { /* Page fault */
#ifdef DEBUG_MESSAGES
            fprintf(stderr, "\e[0;33mProcessed SIGUSR1\e[m\n");
#endif /* DEBUG_MESSAGES */
            signal_number = 0;
        } else if (signal_number == SIGUSR2) { /* PT dump */
#ifdef DEBUG_MESSAGES
            fprintf(stderr, "\e[0;33mProcessed SIGUSR2\e[m\n");
#endif /* DEBUG_MESSAGES */
            signal_number = 0;
        } else if (signal_number == SIGINT) {
#ifdef DEBUG_MESSAGES
            fprintf(stderr, "\e[0;33mProcessed SIGINT\e[m\n");
#endif /* DEBUG_MESSAGES */
        }
    }

    return 0;
}

/* Your code goes here... */

void sighandler(int signo) {
    // What should I do?
    signal_number = signo;

    if (signal_number == SIGUSR1) {//PAGEFAULT
#ifdef DEBUG_MESSAGES
        fprintf(stderr, "\e[0;33ma pagefault signal was received\e[m\n");
#endif /* DEBUG_MESSAGES */

        vmem->adm.pf_count++; //pagefault counter erhöhen
        allocate_page();
        sem_post(&(vmem->adm.sema));

    } else if (signal_number == SIGUSR2) {//PAGETABLEDUMB

#ifdef DEBUG_MESSAGES
        fprintf(stderr, "\e[0;33ma dump signal was received\e[m\n");
#endif /* DEBUG_MESSAGES */

        //printf("PAGETABLE DUMP!\n");
        dump_pt();

    } else if (signal_number == SIGINT) {//INTERRUPT Shutdown
#ifdef DEBUG_MESSAGES
        fprintf(stderr, "\e[0;33ma shutdown signal was received\e[m\n");
#endif /* DEBUG_MESSAGES */

        printf("\n[MMANAGE] Shutting down.. \n");
        // dump_pt();
        cleanup();
        exit(signal_number);

    } else {
#ifdef DEBUG_MESSAGES
        fprintf(stderr, "\e[0;33ma unknown signal was received\e[m\n");
#endif /* DEBUG_MESSAGES */

        printf("[ERROR] UNKNOWN SIGNAL\n");
    }
}

void vmem_init(void) {
    int shmID = 0;
    int i = 0;
    unsigned int ui = 0;

    if (shmdt(vmem) == -1) {
#ifdef DEBUG_MESSAGES
        fprintf(stderr, "\e[1;33mCould not detach vmem - nothing wrong\e[m\n");
    } else {
        fprintf(stderr, "\e[1;32mold vmem detach successful\e[m\n");
#endif /* DEBUG_MESSAGES */
    }

    if (shmctl(SHMKEY, IPC_RMID, 0) != -1) {
#ifdef DEBUG_MESSAGES
        fprintf(stderr, "\e[1;33mCould not destroy vmem - nothing wrong\e[m\n");
    } else {
        fprintf(stderr, "\e[1;32mold vmem destroyed successful\e[m\n");
#endif /* DEBUG_MESSAGES */
    }

    shmID = shmget(SHMKEY, SHMSIZE, IPC_CREAT | SHMPERMISSION);
    if (shmID < 0) {
        perror("\e[0;31mError initializing vmem shared Memory\e[m");
        exit(EXIT_FAILURE);
    }
#ifdef DEBUG_MESSAGES
    else {
        fprintf(stderr, "\e[1;32mshm successfully allocated\e[m\n");
    }
#endif /* DEBUG_MESSAGES */

    vmem = shmat(shmID, NULL, 0);
    if (vmem == (struct vmem_struct *) - 1) {
        perror("\e[0;31mError attach vmem struct\e[m");
        exit(EXIT_FAILURE);
    }
#ifdef DEBUG_MESSAGES
    else {
        fprintf(stderr, "\e[1;32mshm successfully attached\e[m\n");
    }
#endif /* DEBUG_MESSAGES */

    vmem->adm.size = SHMSIZE;
    vmem->adm.mmanage_pid = getpid();
    vmem->adm.shm_id = SHMKEY;

    //Semaphore init
    if (sem_init(&(vmem->adm.sema), 1, 0) == -1) {
        perror("\e[0;31mError initializing semaphore!\e[m");
        exit(EXIT_FAILURE);
    }
#ifdef DEBUG_MESSAGES
    else {
        fprintf(stderr, "\e[1;32mSemaphore initialized successfully! \e[m\n");
    }
#endif /* DEBUG_MESSAGES */

    vmem->adm.req_pageno = VOID_IDX;
    vmem->adm.next_alloc_idx = 0;
    vmem->adm.pf_count = 0;
    vmem->adm.g_count = 0;

    for (ui = 0; ui < VMEM_BMSIZE; ui++) {
        vmem->adm.bitmap[ui] = 0;
    }

    for (i = 0; i < VMEM_NPAGES; i++) {
        vmem->pt.entries[i].count = HIGHEST_AGE_COUNTER_BIT;
        vmem->pt.entries[i].flags = 0;
        vmem->pt.entries[i].frame = VOID_IDX;
    }

    for (i = 0; i < VMEM_NFRAMES; i++) {
        vmem->pt.framepage[i] = VOID_IDX;
    }

    for (i = 0; i < (VMEM_NFRAMES * VMEM_PAGESIZE); i++) {
        vmem->data[i] = 0;
    }

#ifdef DEBUG_MESSAGES
    fprintf(stderr, "\e[1;32mvmem successfully initialized\e[m\n");
#endif /* DEBUG_MESSAGES */
}

void allocate_page(void) {
    int frame = find_remove_frame();
    int currentPageOfFrame;

    if (frame == -1) {
        perror("\e[0;31mFehler beim Finden eines freies Frames\e[m");
        exit(EXIT_FAILURE);
    }

    if (vmem->adm.req_pageno == -1) {
        perror("\e[0;31mPage -1 kann nicht allociert werden\e[m");
        exit(EXIT_FAILURE);
    }

    currentPageOfFrame = vmem->pt.framepage[frame];

#ifdef DEBUG_MESSAGES
    fprintf(stderr, "replace content in         \e[1;35mFrame %2d\e[m: \e[1;34mcurrentPage %10d newPage %10d\e[m\n", frame, vmem->pt.framepage[frame], vmem->adm.req_pageno);
#endif /* DEBUG_MESSAGES */
    logThisEvent(currentPageOfFrame, frame);

    if (currentPageOfFrame != -1) {
        store_page(currentPageOfFrame);
    }

    currentPageOfFrame = vmem->adm.req_pageno;
    update_pt(frame);
    fetch_page(currentPageOfFrame);
    vmem->adm.req_pageno = -1;

    /* Wer übertriebene DEBUG MESSAGES braucht -> einkommentieren
        #ifdef DEBUG_MESSAGES
            fprintf(stderr, "replaced content in        \e[1;35mFrame %2d\e[m: \e[1;34mcurrentPage %10d\e[m\n", frame, vmem->pt.framepage[frame]);
            fprintf(stderr, "Frame of \e[1;34mPage %10d\e[m:  \e[1;35mFrame %2d\e[m\n", currentPageOfFrame, vmem->pt.entries[currentPageOfFrame].frame);

        dump_pt();

        #endif
     */
}

void fetch_page(int pt_idx) {
    //Page in freien Frame laden
    int frameIndexToLoadPageInto = vmem->pt.entries[pt_idx].frame;
    int frameOffset = frameIndexToLoadPageInto * VMEM_PAGESIZE;
    int i = 0;

#ifdef DEBUG_MESSAGES
    fprintf(stderr, "Fetch \e[1;34mpage %10d\e[m into \e[1;35mFrame %2d\e[m\n", pt_idx, frameIndexToLoadPageInto);
#endif /* DEBUG_MESSAGES */

    fseek(pagefile, pt_idx * VMEM_PAGESIZE * sizeof (int), SEEK_SET);
    for (i = 0; i < VMEM_PAGESIZE; i++) {
        if (fread(&vmem->data[frameOffset + i], sizeof (int), 1, pagefile) != 1) {
            perror("\e[0;31mFehler beim Schreiben in der Pagefile\e[m");
            exit(EXIT_FAILURE);
        }
    }

    //vmem->adm.bitmap[pt_idx] = 1;
    vmem->pt.entries[pt_idx].flags = PTF_PRESENT;
    vmem->pt.entries[pt_idx].frame = frameIndexToLoadPageInto;
    vmem->pt.framepage[frameIndexToLoadPageInto] = pt_idx;
}

void store_page(int pt_idx) {
    //Page wegspeichern, wenn verändert
    int frameOfPage = vmem->pt.entries[pt_idx].frame;
    int frameOffset = frameOfPage * VMEM_PAGESIZE;
    int i = 0;

#ifdef DEBUG_MESSAGES
    fprintf(stderr, "Store \e[1;34mpage %10d\e[m from \e[1;35mFrame %2d\e[m\n", pt_idx, frameOfPage);
#endif /* DEBUG_MESSAGES */

    if (frameOfPage == -1) {
        perror("\e[0;31mError store not loaded Frame\e[m");
        exit(EXIT_FAILURE);
    }

    if (vmem->pt.entries[pt_idx].flags & PTF_DIRTY) {
#ifdef DEBUG_MESSAGES
        fprintf(stderr, "something changed in frame - save into pagefile\n");
#endif /* DEBUG_MESSAGES */

        fseek(pagefile, pt_idx * VMEM_PAGESIZE * sizeof (int), SEEK_SET);
        for (i = 0; i < VMEM_PAGESIZE; i++) {
            if (fwrite(&vmem->data[frameOffset + i], sizeof (int), 1, pagefile) != 1) {
                perror("\e[0;31mFehler beim Schreiben in der Pagefile\e[m");
                exit(EXIT_FAILURE);
            }
        }
    }

    //vmem->adm.bitmap[pt_idx] = 0;
    vmem->pt.entries[pt_idx].flags = 0;
    vmem->pt.entries[pt_idx].frame = -1;
    if (VMEM_ALGO == VMEM_ALGO_AGING) {
        vmem->pt.entries[pt_idx].count = HIGHEST_AGE_COUNTER_BIT; //wichtig für aging
    }
    vmem->pt.framepage[frameOfPage] = -1;
}

void update_pt(int frame) {
    vmem->pt.entries[vmem->adm.req_pageno].frame = frame;
}

int find_remove_frame(void) {
    int frame = -1;

    switch (VMEM_ALGO) { // wird durchs makefile gesetzt
        case VMEM_ALGO_FIFO:
            frame = find_remove_fifo();
            break;
        case VMEM_ALGO_AGING:
            frame = find_remove_aging();
            break;
        case VMEM_ALGO_CLOCK:
            frame = find_remove_clock();
            break;
        default:
            frame = -1;
            break;
    }

    if (frame == -1) {
        perror("\e[0;31mFrame -1 kann nicht entfernt werden\e[m");
        exit(EXIT_FAILURE);
    }

    return frame;
}

int find_remove_fifo(void) {
    int frameToRemove = vmem->adm.next_alloc_idx;
    vmem->adm.next_alloc_idx++;
    vmem->adm.next_alloc_idx = vmem->adm.next_alloc_idx % VMEM_NFRAMES;

    return frameToRemove;
}

int find_remove_clock(void) {
    int frameToReturn = 0;

    if ((vmem->pt.framepage[vmem->adm.next_alloc_idx] != -1)) {
        while ((vmem->pt.entries[vmem->pt.framepage[vmem->adm.next_alloc_idx]].flags & PTF_REF) == PTF_REF) {
            vmem->pt.entries[vmem->pt.framepage[vmem->adm.next_alloc_idx]].flags &= ~PTF_REF;
            vmem->adm.next_alloc_idx = (vmem->adm.next_alloc_idx + 1) % VMEM_NFRAMES;
        }
    }
    frameToReturn = vmem->adm.next_alloc_idx;
    vmem->adm.next_alloc_idx = (vmem->adm.next_alloc_idx + 1) % VMEM_NFRAMES;

    return frameToReturn;
}

int find_remove_aging(void) {
    // find least used page in frames
    int i;
    int currentPage = 0;
    int leastUsedPageInFrame = 0;
    int age = INT_MAX;
    for (i = 0; i < VMEM_NFRAMES; i++) {
        currentPage = vmem->pt.framepage[i];
        if ((vmem->pt.framepage[currentPage] != -1)) {
            if (age > vmem->pt.entries[currentPage].count) {
                age = vmem->pt.entries[currentPage].count;
                leastUsedPageInFrame = i;
            }
        } else {
            leastUsedPageInFrame = i;
        }
    }

    return leastUsedPageInFrame;
}

int search_bitmap(void) {
    // What should I do?
    return -1;
}

int find_free_bit(Bmword bmword, Bmword mask) {
    int i;
    for (i = 0; i < VMEM_NFRAMES; i++) {
        if (vmem->adm.bitmap[i] == 0) {
            return i;
        }
    }
    i = bmword | mask;
    return -1; //kein freier frame
}

void init_pagefile(const char *pfname) {
    int i;
    int val;
    srand(SEED_PF);
    // Pagefile erstellen

    if ((pagefile = fopen(pfname, "w+b")) == NULL) {
        perror("\e[0;31mError open pagefile\e[m");
        exit(EXIT_FAILURE);
    }
#ifdef DEBUG_MESSAGES
    fprintf(stderr, "\e[1;32mpagefile successfully opened\e[m\n");
#endif /* DEBUG_MESSAGES */

    fseek(pagefile, 0, SEEK_SET);
    for (i = 0; i < VMEM_VIRTMEMSIZE; i++) {
        val = rand();
        if (fwrite(&val, sizeof (int), 1, pagefile) != 1) {
            perror("\e[0;31mError write to pagefile failed!\e[m");
            exit(EXIT_FAILURE);
        }
    }

#ifdef DEBUG_MESSAGES
    fprintf(stderr, "\e[1;32mpagefile successfully initialized\e[m\n");
#endif /* DEBUG_MESSAGES */
}

void cleanup(void) {
    int currentPageIdx = 0;
    int i;

#ifdef DEBUG_MESSAGES
    dump_pt();
#endif /* DEBUG_MESSAGES */

    //store dirty pages
    for (i = 0; i < VMEM_NFRAMES; i++) {
        currentPageIdx = vmem->pt.framepage[i];
        if (currentPageIdx == -1)
            continue;

        store_page(currentPageIdx);
        //logThisEvent(currentPageIdx, i);
    }

    //destroy semaphore
    sem_post(&(vmem->adm.sema));
    if (sem_destroy(&(vmem->adm.sema)) == -1) {
#ifdef DEBUG_MESSAGES
        fprintf(stderr, "\e[0;31mERROR: Could not destroy semaphore\e[m\n");
#endif /* DEBUG_MESSAGES */
    }
#ifdef DEBUG_MESSAGES
    else {
        fprintf(stderr, "\e[1;32mSemaphore successfully destroyed\e[m\n");
    }
#endif /* DEBUG_MESSAGES */

    //close files
    if (fclose(pagefile) != 0) {
#ifdef DEBUG_MESSAGES
        fprintf(stderr, "\e[0;31mFailed to close pagefile.bin\e[m\n");
#endif /* DEBUG_MESSAGES */
    }
    if (fclose(logfile) != 0) {
#ifdef DEBUG_MESSAGES
        fprintf(stderr, "\e[0;31mFailed to close logfile\e[m\n");
#endif /* DEBUG_MESSAGES */
    }

    //unlink memory
    if (shmdt(vmem) == -1) {
#ifdef DEBUG_MESSAGES
        fprintf(stderr, "\e[0;31mError detach vmem\e[m\n");
    } else {
        fprintf(stderr, "\e[1;32mvmem detached successful\e[m\n");
#endif /* DEBUG_MESSAGES */
    }

    if (shmctl(SHMKEY, IPC_RMID, 0) != -1) {
#ifdef DEBUG_MESSAGES
        fprintf(stderr, "\e[0;31mError destroy vmem\e[m\n");
    } else {
        fprintf(stderr, "\e[1;32mvmem destroyed successful\e[m\n");
#endif /* DEBUG_MESSAGES */
    }
#ifdef DEBUG_MESSAGES
    fprintf(stderr, "\e[1;33mcleanup finished\e[m\n");
#endif /* DEBUG_MESSAGES */
}

void dump_pt(void) {
    // Dump der Pagetable erstellen... wohin?
    int y, x, currentPage;
    printf("\e[1;33m#########################################DUMP#########################################\n\e[m");
    for (y = 0; y < VMEM_NFRAMES; y++) {
        currentPage = vmem->pt.framepage[y];
        printf("\e[1;35mFrame %2d\e[m: \e[1;34mPage %3d\e[m  ",
                y, vmem->pt.framepage[y]);
        if (currentPage != -1) {
            printf("Flags (P D R): (%d %d %d)  ",
                    vmem->pt.entries[currentPage].flags & PTF_PRESENT,
                    vmem->pt.entries[currentPage].flags & PTF_DIRTY,
                    vmem->pt.entries[currentPage].flags & PTF_REF);

            if (vmem->pt.entries[currentPage].frame != y)
                printf("\e[0;31m");
            printf("Frame der Page: %2d\e[m  ",
                    vmem->pt.entries[currentPage].frame);

            printf("Page count (Age): %4d",
                    vmem->pt.entries[currentPage].count);
        }
        printf("\n");
    }
    printf("\nDATA:\n");
    for (y = 0; y < VMEM_NFRAMES; y++) {
        printf("\e[1;35mFrame %2d\e[m: ", y);
        for (x = 0; x < VMEM_PAGESIZE; x++) {
            if (x % 8 == 0 && x / 8 > 0)
                printf("          ");
            printf("%010d ", vmem->data[y * VMEM_PAGESIZE + x]);
            if (x % 8 == 7)
                printf("\n");
        }
    }
#ifdef DEBUG_MESSAGES
    fprintf(stderr, "dumped everything I got\n");
#endif /* DEBUG_MESSAGES */
}

void logThisEvent(int replaced_page, int alloc_frame) {
    struct logevent le = {-1, -1, -1, -1, -1};
    le.req_pageno = vmem->adm.req_pageno;
    le.replaced_page = replaced_page;
    le.alloc_frame = alloc_frame;
    le.pf_count = vmem->adm.pf_count;
    le.g_count = vmem->adm.g_count;

    logger(le);
}

/* Do not change!  */
void
logger(struct logevent le) {
    fprintf(logfile, "Page fault %10d, Global count %10d:\n"
            "Removed: %10d, Allocated: %10d, Frame: %10d\n",
            le.pf_count, le.g_count,
            le.replaced_page, le.req_pageno, le.alloc_frame);
    fflush(logfile);
}

