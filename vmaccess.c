#include "vmaccess.h"
#include "vmem.h"
#include "mmanage.h"

struct vmem_struct *vmem = NULL;

int pageOfAddress(int address) {
    return address / VMEM_PAGESIZE;
}

int addressInPage(int address) {
    return address % VMEM_PAGESIZE;
}

void vm_init(void) {
    int shmID = 0;

    shmID = shmget(SHMKEY, SHMSIZE, SHMPERMISSION);
    if (shmID < 0) {
        perror("\e[0;31mFailed to allocate shm\e[m");
        exit(EXIT_FAILURE);
    }
#ifdef DEBUG_MESSAGES
    else {
        printf("\e[1;32mshm successfully allocated\e[m\n");
    }
#endif

    vmem = shmat(shmID, NULL, 0);
    if (vmem == (void*) - 1) {
        perror("\e[0;31mFailed to attach shm\e[m");
        exit(EXIT_FAILURE);
    }
#ifdef DEBUG_MESSAGES
    else {
        printf("\e[1;32mshm successfully attached\e[m\n");
    }
#endif
}

void agingUpdateReferenceBits(void) {

    int currentPage;
    int i;
    for (i = 0; i < VMEM_NFRAMES; i++) {
        currentPage = vmem->pt.framepage[i];

        if (currentPage != -1) {
            vmem->pt.entries[currentPage].count /= 2; // Bitshift um 1 nach rechts

            if (vmem->pt.entries[currentPage].flags & PTF_REF) { //wenn ref gesetzt
                vmem->pt.entries[currentPage].count += (HIGHEST_AGE_COUNTER_BIT); // REF bit wird an vorderster stelle angehängt (8Bit))
                vmem->pt.entries[currentPage].flags &= ~(PTF_REF); // reference bit auf 0 setzen
            }
        }
    }
}

void updateGlobalCounter(void) {
    vmem->adm.g_count++; //globalen access counter erhöhen
    if (VMEM_ALGO == VMEM_ALGO_AGING) { // wird nur bei aging ausgeführt
        if (vmem->adm.g_count % AGING_AGE == 0) { // "Zeit"-interval abgelaufen
            agingUpdateReferenceBits();
        }
    }
}

int pageFault(int pt_idx) {
    int frame = -1;

    vmem->adm.req_pageno = pt_idx;

    if (kill(vmem->adm.mmanage_pid, SIGUSR1) == -1) {
        perror("Failed to send signal USR1");
        exit(EXIT_FAILURE);
    }
    sem_wait(&(vmem->adm.sema));

    frame = vmem->pt.entries[pt_idx].frame;

#ifdef DEBUG_MESSAGES
    fprintf(stderr, "Load Page Into Frame:            \e[1;34mPage %10d\e[m at \e[1;35mFrame %2d\e[m\n", pt_idx, frame);
#endif /* DEBUG_MESSAGES */

    return frame;
}

int getPositionInDataArray(int address, int setDirty) {
    int page;
    int pagePresent;
    int localAddressInPage;
    int frame;
    int frameOffset;
    int currentGCount = -1;

    if (vmem == NULL)
        vm_init();

    page = pageOfAddress(address);
    pagePresent = vmem->pt.entries[page].flags & PTF_PRESENT;
    localAddressInPage = addressInPage(address);
    frame = vmem->pt.entries[page].frame;
    currentGCount = vmem->adm.g_count;

#ifdef DEBUG_MESSAGES
    fprintf(stderr, "Access   \e[1;36maddress %10d\e[m from \e[1;34mPage %10d\e[m (present %1d at \e[1;35mFrame %2d\e[m) - \e[0;34mg_count: %10d\e[m\n", address, page, pagePresent, frame, vmem->adm.g_count);
#endif /* DEBUG_MESSAGES */

    if (frame == -1 || !pagePresent) {
        if ((frame == -1 && pagePresent) ||
                (frame != -1 && !pagePresent)) {
            if (kill(vmem->adm.mmanage_pid, SIGUSR2) == -1) {
                perror("Failed to send signal USR2");
                exit(EXIT_FAILURE);
            }
            perror("\e[0;31m[ERROR] Present Bit und Frame -1 -> irgendwas stimmt da nicht\e[m");
            exit(EXIT_FAILURE);
        }
        frame = pageFault(page);
    }

    vmem->pt.entries[page].flags |= PTF_REF;
    if (setDirty)
        vmem->pt.entries[page].flags |= PTF_DIRTY;

    updateGlobalCounter();

    if (vmem->adm.g_count != currentGCount + 1) {
        perror("\e[0;31mG_Count is counting strange\e[m");
        exit(EXIT_FAILURE);
    }

    //frame = vmem->pt.entries[page].frame;
    frameOffset = frame * VMEM_PAGESIZE;

    return frameOffset + localAddressInPage;
}

int vmem_read(int address) {
    int positionInDataArray;
    int data;

    positionInDataArray = getPositionInDataArray(address, FALSE);
    data = vmem->data[positionInDataArray];

#ifdef DEBUG_MESSAGES
    fprintf(stderr, "Read  on \e[1;36maddress %10d\e[m \e[0;36mvalue %10d\e[m\n", address, data);
#endif /* DEBUG_MESSAGES */

    return data;
}

void vmem_write(int address, int data) {
    int positionInDataArray;

    positionInDataArray = getPositionInDataArray(address, TRUE);
    vmem->data[positionInDataArray] = data;

#ifdef DEBUG_MESSAGES
    fprintf(stderr, "Wrote on \e[1;36maddress %10d\e[m \e[0;36mvalue %10d\e[m\n", address, data);
#endif /* DEBUG_MESSAGES */
}

void stop_mmanage(void) {
    if (kill(vmem->adm.mmanage_pid, SIGINT) == -1) {
        perror("\e[0;31mFailed to send signal SIGINT\e[m");
        exit(EXIT_FAILURE);
    }
#ifdef DEBUG_MESSAGES
    fprintf(stderr, "\e[1;32mSuccessfully send SIGINT signal to mmanage\e[m\n");
#endif /* DEBUG_MESSAGES */

}
