#ifndef VMTESTAPP_H
#define	VMTESTAPP_H

#include <stdio.h>
#include <stdlib.h>
#include "vmaccess.h"

#define SEED 2806
#define LENGTH 550
#define RNDMOD 1000

void swap(int addr1, int addr2);

#endif	/* VMTESTAPP_H */

